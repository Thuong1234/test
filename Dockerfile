FROM node:14-stretch-slim as build
WORKDIR /app
COPY . /app

RUN npm set cache $CI_PROJECT_DIR

RUN npm ci

FROM nginx:latest
COPY --from=build /app/build /usr/share/nginx/html